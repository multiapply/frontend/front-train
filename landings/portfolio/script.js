// Mouse Circle
/** @type {HTMLElement} */
const mouseCircle = document.querySelector('.mouse-circle');
/** @type {HTMLElement} */
const mouseDot = document.querySelector('.mouse-dot');

/**
 * 
 * @param {number} x Horizontal position
 * @param {number} y Vertical position
 */
const mouseCircleFn = (x,y) => { 
  mouseCircle.style.cssText = `top:${y}px; left:${x}px; display: block`;
  mouseDot.style.cssText = `top:${y}px; left:${x}px; display: block`;
};



const hideCircleCallback = () => { 
  mouseCircle.style.display='none';
  mouseDot.style.display='none';
}
// Animated Circles
/** @type {NodeList} Additional Hero Images*/
const circles = document.querySelectorAll('.circle');
/** @type {HTMLElement} Center Hero Image*/
const mainImg = document.querySelector('.main-circle img');

let prevX = 0;
let prevY = 0;
const circleAxisMovement=40;
/**
 * 
 * @param {MouseEvent} e 
 * @param {Number} x 
 * @param {Number} y 
 */
const animateCircles = (e,x,y) => { 
  if(x<prevX){
    circles.forEach((circle)=> {
      circle.style.left=`${circleAxisMovement}px`;
    });
    mainImg.style.left=`${circleAxisMovement}px`;
  }else if(x>prevX){
    circles.forEach((circle)=> {
      circle.style.left=`-${circleAxisMovement}px`;
    });
    mainImg.style.left=`-${circleAxisMovement}px`;
  }
  if(y<prevY){
    circles.forEach((circle)=> {
      circle.style.top=`${circleAxisMovement}px`;
    });
    mainImg.style.top=`${circleAxisMovement}px`;
  }else if(y>prevY){
    circles.forEach((circle)=> {
      circle.style.top=`-${circleAxisMovement}px`;
    });
    mainImg.style.top=`-${circleAxisMovement}px`;
  }
  prevX=e.clientX
  prevY=e.clientY
}

/**
 * 
 * @param {MouseEvent} event 
 */
function moveCircleCallback(event) {
  /** @type {number} */
  let x = event.clientX;
  /** @type {number} */
  let y = event.clientY;
  mouseCircleFn(x,y);
  animateCircles(event,x,y);
};


// Main button
/** @type {HTMLElement} */
const mainBtn = document.querySelector('.main-btn')

/** @type {HTMLElement} */
let ripple;

/**
 * 
 * @param {MouseEvent} e 
 */
const handleMainBtnRipple = (e) => { 
  // console.log(e.target(getBoundingClientRect()))
  const left = e.clientX - e.target.getBoundingClientRect().left;
  const top = e.clientY - e.target.getBoundingClientRect().top;

  ripple = document.createElement("div");
  ripple.classList.add("ripple");
  ripple.style.left = `${left}px`;
  ripple.style.top = `${top}px`;
  mainBtn.prepend(ripple);
}

const removeRipple = () => { 
  mainBtn.removeChild(ripple);
}

// document.addEventListener("DOMContentLoaded", function() {});
document.body.addEventListener("mousemove", moveCircleCallback);
document.body.addEventListener("mouseleave", hideCircleCallback);

mainBtn.addEventListener("mouseenter", handleMainBtnRipple)
mainBtn.addEventListener("mouseleave", removeRipple)