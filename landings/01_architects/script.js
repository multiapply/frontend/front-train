const menuButton = document.querySelector('.menu-button');
const mainContainer = document.querySelector(".container");
function toggleMenu() {
  mainContainer.classList.toggle("open-menu");
}

menuButton.addEventListener("click",toggleMenu);