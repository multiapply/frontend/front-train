import { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import Logo from "../assets/logo.png";
import './Header.css'

export const Header = () => {
  const [theme, setTheme] = useState(JSON.parse(localStorage.getItem("theme")) || "dark");

  useEffect(()=>{
    localStorage.setItem("theme", JSON.stringify(theme))
    document.documentElement.removeAttribute("class")
    document.documentElement.classList.add(theme)
  }, [theme]);

  const appliedTheme = (base) => { 
    return theme === base ? `${base} activeTheme` : base
    // if (theme === base) {
    //   return `${base} activeTheme`
    // } else {
    //   return base
    // }
   }

  return (

    <header>
      <img className="logo" src={Logo} alt="Taskmate Logo"/>
      <nav>
        <ul className="menu-list">
          { routes.map(route => (
            <li key={route.to}>
              <NavLink
                style={({isActive}) => ({
                  color: isActive ? 'red' : 'blue',
                })}
                to={route.to}
              >
                {route.text}</NavLink>
            </li>
          ))}
        </ul>
      </nav>
      <div className="themeSelector">
        <span onClick={() => setTheme("light")} className={appliedTheme("light")}></span>
        <span onClick={() => setTheme("medium")} className={appliedTheme("medium")}></span>
        <span onClick={() => setTheme("dark")} className={appliedTheme("dark")}></span>
        <span onClick={() => setTheme("gOne")} className={appliedTheme("gOne")}></span>
        <span onClick={() => setTheme("gTwo")} className={appliedTheme("gTwo")}></span>
        <span onClick={() => setTheme("gThree")} className={appliedTheme("gThree")}></span>

      </div>
    </header>
  )
}


const routes = [];
routes.push({
  to: '/',
  text: 'TaskMate (Home)',
});
routes.push({
  to: '/machine',
  text: 'machine',
});
routes.push({
  to: '/shopmate',
  text: 'shopmate',
});
routes.push({
  to: '/counter',
  text: 'counter',
});
routes.push({
  to: '/concept',
  text: 'concept',
});
routes.push({
  to: '/appoint',
  text: 'appoint',
});