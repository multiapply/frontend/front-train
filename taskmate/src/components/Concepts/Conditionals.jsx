
export const Conditionals = () => {
  function firstCondition(first, second) {
    var condition = first;
    var anotherCondition = second;
    if (condition) {
      return 'Executed'
    } else if (anotherCondition){
      return 'still Executed'
    } else {
      return 'Not Executed'
    }
  }

  function handleLuckyNumber(number) {
    var luckyNumber = number;
    var result = '';
    switch (luckyNumber) {
      case 1:
        result = 'Is 1'
        break;
      case 8:
        result = 'Is 8'
        break;
      default:
        result = 'Default'
        break;
    }
    return result;
    
  }
  return (

    <section>
      <h4>Concept: Conditionals</h4>
      <p>{firstCondition(true,true)}</p>
      <p>{firstCondition(false,true)}</p>
      <p>{firstCondition(false,false)}</p>
      <h4>1 is interpreted as true, 0 as false</h4>
      <p> (automatic casting)</p>
      <p>{firstCondition(1,false)}</p>
      <p>{firstCondition(0,false)}</p>
      <p>{firstCondition(0,2)}</p>
      <h4>validate other true equivalence</h4>
      <p>{firstCondition('false',false)}</p>
      <p>{firstCondition(null,false)}</p>

      <h4>Concept: switch statement</h4>
      <p>{handleLuckyNumber(8)}</p>
      <p>{handleLuckyNumber(7)}</p>


      
    </section>
  )
}
