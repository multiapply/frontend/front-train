export const Variables = () => {
  var variable = 5;
  var text = 'text';
  var boolean = true;
  var floats = 6.5;

  var arr = [1,2,3, "4"];

  var objecti = {
    name: 'Max'
  };
  
  return (
    <section>
      <h4>Variables</h4>
      <article>
        <h4> Concept: Variables</h4>
        <p>num: {variable}</p>
        <p>txt: {text}</p>
        <p>bool: {boolean}</p>
        <p>6.5 is a: {typeof floats}</p>
        <p>5 is a: {typeof variable}</p>
        <p>'text' is a: {typeof text}</p>

        <p>bool is a: {typeof boolean}</p>
      </article>

      <article>
        <h4> Concept: Undefined, null, NaN</h4>
        <p>arr: {arr}</p>
        <p>arr: {arr[0]}</p>
        <p>[1,2,3] is a: {typeof arr}</p>
        <p> multiple values in 1 var</p>
      </article>

      <article>
        <h4> Concept: Objects</h4>
        <p>object: {objecti.name}</p>
        <p>object is a: {typeof objecti}</p>
      </article>
    </section>
  )
}


