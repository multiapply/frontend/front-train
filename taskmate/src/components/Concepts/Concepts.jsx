import { Variables } from "./Variables"
import { Functions } from "./Functions"
import { Conditionals } from "./Conditionals"
import { Iterations } from "./Iterations"

export const Concepts = () => {
  return (
    <section className="box">
      <h1>Concepts</h1>
      {/* <Variables /> */}
      {/* <Functions /> */}
      {/* <Conditionals /> */}
      <Iterations />
    </section>
  )
}
