
export const Functions = () => {

  function getString() {
    return 'Inside function'
  }
  function calc(num1, num2) {
    let stringVar = num1 + num2;
    return stringVar;
  }

  return (
    <section>
      <h4>Concept: Functions</h4>
      <p>{getString()}</p>
      <p>{calc(10,8)}</p>
      
    </section>
  )
}
