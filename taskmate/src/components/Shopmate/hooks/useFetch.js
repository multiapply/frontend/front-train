import { useState, useEffect, useCallback } from "react";

export const useFetch = (url) => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");

  const fetchData = useCallback(async (signal) => { 
    setLoading(true);
    try {
      const response = await fetch(url, signal);
      if(!response.ok){
        throw new Error(response.statusText);
      }
      const result = await response.json();
      setLoading(false);
      setData(result);
      setError("");
    } catch (error){
      setLoading(false);
      setError(error.message);
    }
  }, [url]);

  useEffect(()=>{
    const controller = new AbortController();
    fetchData( { signal: controller.signal});
    return () => controller.abort();
  }, [fetchData]);

  
  return { data, loading, error }
}
