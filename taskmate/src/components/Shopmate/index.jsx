import { ProductList } from "./ProductList"

export const Shopmate = () => {
  return (
    <div>
      <h1>Product List</h1>
      <ProductList />
    </div>
  )
}
