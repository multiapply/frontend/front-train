import { useState } from "react";

import "./ProductList.css"
import { useFetch } from "./hooks/useFetch";

export const ProductList = () => {
  const [url, setUrl] = useState("/products");

  const { data: products, loading, error } = useFetch(url);


  const loadingMessage = (
    <p>Loading products...</p>
  )
  const errorMessage = (
    <p>{error}</p>
  )
  return (
    <section>
      <div className="filter">
        <button onClick={()=> setUrl("/products")}>All</button>
        <button onClick={()=> setUrl("/products?in_stock=true")}>In Stock</button>
      </div>
      { loading && loadingMessage }
      { error && errorMessage }
      { products && products.map((product) => (
        <div className="card" key={product.id}>
          <p className="card-id">{product.id}</p>
          <p className="card-name">{product.name}</p>
          <p className="card-info">
          <span>${product.price}</span>
          <span 
            className={product.in_stock ? "instock": "unavailable"} >
              {product.in_stock ? "In Stock": "Unavailable"}
          </span>
          </p>
        </div>
      ))}
    </section>
  )
}
