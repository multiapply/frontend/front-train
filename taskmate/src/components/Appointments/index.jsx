import { AppointmentsDayView } from './Appointment';
import { sampleAppointments } from './sampleData';

export const Appointments = () => {
  return (
    <AppointmentsDayView appointments={sampleAppointments} />
  )
}
