import { TodoProvider } from "./TodoContext";

import { TodoMachineUI } from "./TodoMachineUI";

export const TodoMachine = () => {
  
  return (
    <TodoProvider>
      <TodoMachineUI />
    </TodoProvider>
  )
}
