import { useContext, useState } from 'react'
import { TodoContext } from '../TodoContext'
import './TodoForm.css'

export const TodoForm = () => {
  const [newTodoValue, setNewTodoValue] = useState('')
  const {addTodo,setOpenModal,} = useContext(TodoContext);

  const handleChange = (event) => { 
    setNewTodoValue(event.target.value)
  }

  const handleCancel = () => { 
    setOpenModal(false);
  }
  const handleSubmit = (event) => { 
    event.preventDefault();
    if(newTodoValue.length<=0) return;
    addTodo(newTodoValue);
    handleCancel();
  }
  const handleKeyDown = (event) => { 
    if (event.charCode === 13){
      handleSubmit(event);
    }
  }

  return (
    <form onSubmit={handleSubmit} onKeyPress={handleKeyDown}>
      <label>Escribe tu nuevo TODO</label>
      <textarea 
        value={newTodoValue}
        onChange={handleChange}
        placeholder='Describir el TODO'
      />
      <div className='TodoForm-buttonContainer'>
        <button className='TodoForm-button TodoForm-button--cancel'
          type="button" onClick={handleCancel}> Cancel</button>
        <button className='TodoForm-button TodoForm-button--add'
          type="submit" disabled={!newTodoValue}> Add</button>
      </div>
    </form>
  )
}
