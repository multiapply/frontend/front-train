import { useContext } from "react";

import { TodoContext } from "./TodoContext"

import { TodoCounter } from "./TodoCounter"
import { TodoSearch } from "./TodoSearch"
import { TodoList } from "./TodoList"
import { TodoItem } from "./TodoItem"
import { CreateTodoButton } from "./CreateTodoButton"

import { Modal } from "./Modal";
import { TodoForm } from "./TodoForm";

export const TodoMachineUI = () => {
  const {
    error, 
    loading, 
    searchedTodos, 
    toggleCompleteTodo, 
    deleteTodo,
    openModal,
    setOpenModal,
  } = useContext(TodoContext)
  
  const loadingMessage = (
    <p>Estamos Cargando, no desesperes...</p>
  );
  const errorMessage = (
    <p>Desesperate, hubo un error.</p>
  );
  const initialMessage = (
    <p>Creat tu primer TODO!</p>
  );

  const renderModal = (
    <Modal>
      <TodoForm />
    </Modal>
  );

  return (
    <>
      <TodoCounter />
      <TodoSearch />

      <TodoList >
        { error && errorMessage }
        { loading && loadingMessage }
        { (!loading && !searchedTodos.length) && initialMessage }
        {searchedTodos.map(todo => (
          <TodoItem
            key={todo.id}
            text={todo.text}
            completed={todo.completed}
            onComplete={()=> toggleCompleteTodo(todo.text)}
            onDelete={() => deleteTodo(todo.text)}
          />
        ))}
      </TodoList>

      {openModal && renderModal}
      

      <CreateTodoButton 
        setOpenModal={setOpenModal}
      />
    </>
  )
}
