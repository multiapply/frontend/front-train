import './CreateTodoButton.css'
export const CreateTodoButton = ({setOpenModal}) => {

  const addItem = () => {
    setOpenModal(prevState => !prevState);
  }
  return (
    <button 
      className='CreateTodoButton'
      onClick={addItem}
    >
      +
    </button>
  )
}
