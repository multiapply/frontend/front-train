import { useContext } from 'react';
import { TodoContext } from './TodoContext';
import './TodoSearch.css'

export const TodoSearch = () => {
  const { searchText, setSearch} = useContext(TodoContext)
  const searchValue = (event) => {
    setSearch(event.target.value);
  }
  return (
      <input
        type="text"
        placeholder="app"
        className="TodoSearch"
        value={searchText}
        onChange={searchValue}
      />
  )
}
