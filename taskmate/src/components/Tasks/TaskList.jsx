import { useState } from "react";

import { TaskCard } from "./TaskCard";
// import { BoxCard } from "../BoxCard";
import "./TaskList.css";
import "./AddTask.css";

export const TaskList = ({tasks, setTasks, oneTask, setOneTask}) => {
  const [show, setShow] = useState(true);

  function handleEdit(id){
    setOneTask(tasks.find(task => task.id == id));
  }

  function handleDelete(id){
    setTasks(tasks.filter(task => task.id !== id));
  }

  const taskList = (
    tasks.map ((task) => (
      <TaskCard key={task.id} task={task} handleEdit={handleEdit} handleDelete={handleDelete}/>
    ))
  )

  return (
    <>
    <section className="tasklist">
        <div className="header">
          <h2 className="title"> Task List <span>{tasks.length}</span></h2>
          <button className="clearAll" onClick={()=> setTasks([])}>Clear All</button>
          <button className="trigger" onClick={() => setShow(!show)}>{ show ? "Hide Tasks" : "Show Tasks"}</button>
        </div>
        <ul>
          { show && taskList}
        </ul>
    </section>
    {/* <BoxCard result="success">
      <p className="title">Lorem ipsum dolor sit amet.</p>
      <p className="description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste, quia.</p>
    </BoxCard>

    <BoxCard result="warning">
      <p className="title">Lorem ipsum dolor sit amet.</p>
      <p className="description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste, quia.</p>
      <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quia at repellendus vero placeat itaque.</p>
    </BoxCard> */}
    
    </>
  )
}
