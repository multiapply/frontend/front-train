import { useState, useRef } from 'react';
import './AddTask.css'

export const AddTask = ({tasks, setTasks, oneTask, setOneTask}) => {
  const [progress, setProgress] = useState(false);
  const taskRef = useRef("");

  const handleReset = () => { 
    taskRef.current.value = "";
    setOneTask({})
    setProgress(false);
  }
  const handleSubmit = (event) => { 
    event.preventDefault();
    
    const date = new Date();
    let taskname = taskRef.current.value;
    const formattedTime = `${date.toLocaleTimeString()} ${date.toLocaleDateString()}`
    if(oneTask.id){
      const updateTaskList = tasks.map((task) => (
        task.id == oneTask.id ? {
          id: oneTask.id, 
          name: taskname,
          time: formattedTime,
          completed: oneTask.completed
        } : task
      ));
      setTasks(updateTaskList);
    } else {
      const task = {
        id: date.getTime(),
        name: taskname,
        // name: event.target.task.value,
        time: formattedTime,
        completed: Boolean(progress)
      }
      setTasks([...tasks, task]);
    }
    handleReset();
  }
  return (
    <section className="addtask">
      <form onSubmit={handleSubmit}>
        <input ref={taskRef} type="text" 
            value={oneTask.name || ""}
            onChange={e => setOneTask({...oneTask, name: e.target.value})}
            name="task" id="task" maxLength="25"
            placeholder="Task Name" autoComplete="off"/>
        <select onChange={(e) => setProgress(e.target.value)} 
            value={progress} name="" id="">
          <option value="false">Pending</option>
          <option value="true">Completed</option>
        </select>
        <button type="submit">{ oneTask.id ? "Update" : "Add"}</button>
        <span onClick={handleReset} className="reset">Reset</span>
      </form>
    </section>
  )
}



