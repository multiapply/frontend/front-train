import "./TaskCard.css";
import styles from "./TaskCard.module.css";

export const TaskCard = ({task, handleEdit, handleDelete}) => {
  // 
  return (
    <li className={`${styles.taskcard} ${task.completed ? "completed" : "incomplete"}`}> 
      <p>
        <span>{task.name} </span>
        <span className="time">{task.time}</span>
      </p>
      <i className="bi bi-pencil-square" onClick={() => handleEdit(task.id)} ></i>
      <i className="bi bi-trash" onClick={() => handleDelete(task.id)} ></i>
      {/* <button onClick={() => handleDelete(task.id)} className="delete">Delete</button> */}
    </li>
  )
}
