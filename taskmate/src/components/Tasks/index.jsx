import { useEffect, useState } from "react";

import { TaskList } from "./TaskList";
import { AddTask } from "./AddTask";

export const Tasks = () => {
  const [tasks, setTasks] = useState( JSON.parse(localStorage.getItem("tasks")) || [
    {id: 5271, name: "Record Lectures", time: "2:02:01 AM 9/14/2030", completed: true},
    {id: 7825, name: "Edit React Lectures", time: "10:02:01 AM 9/20/2030", completed: false},
    {id: 8391, name: "Watch Lectures", time: "4:02:01 PM 9/25/2030", completed: false}
  ]);
  const [oneTask, setOneTask] = useState({});

  useEffect(()=>{
    localStorage.setItem("tasks", JSON.stringify(tasks))
  }, [tasks])


  return (
    <main>
      <AddTask  tasks={tasks} setTasks={setTasks} oneTask={oneTask} setOneTask={setOneTask}/>
      <TaskList tasks={tasks} setTasks={setTasks} oneTask={oneTask} setOneTask={setOneTask}/>
    </main>
  )
}
