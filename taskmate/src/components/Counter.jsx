import { useState } from "react";

export const Counter = () => {

  const [count, setCount] = useState(0);


  function handleAdd(){
    setCount(count => count + 1)
    setCount(count => count + 1)
    setCount(count => count + 1)
  }

  function handleSub(){
    setCount(count - 1)
  }

  function handleReset(){
    setCount(0)
  }


  return (
    <section className="box">
      <h1>Counter</h1>
      <p>{count}</p>
      <button onClick={handleAdd} className="add">ADD</button>
      <button onClick={handleSub} className="sub">SUB</button>
      <button onClick={handleReset} className="reset">RESET</button>
    </section>
  )
}
