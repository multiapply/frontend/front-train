import { HashRouter, Routes, Route } from 'react-router-dom';

import { Header } from "./components/Header";
import { Footer } from "./components/Footer";
import { Counter } from "./components/Counter";
import { Tasks } from "./components/Tasks";
import { Concepts } from "./components/Concepts/Concepts";
import { TodoMachine } from "./components/TodoMachine";
import { Appointments } from "./components/Appointments";

import './App.css';
import { Shopmate } from "./components/Shopmate";

const App = () => {

  const loading = false
  if(loading) return <h1>Loading...</h1>

  
  return (
    <>
      <HashRouter>
        <div className="App">
          <Header />
            <Routes>
              <Route path="/" element={<Tasks />}/>
              <Route path="/machine" element={<TodoMachine />}/>
              <Route path="/shopmate" element={<Shopmate />}/>
              <Route path="/counter" element={<Counter />}/>
              <Route path="/concept" element={<Concepts />}/>
              <Route path="/appoint" element={<Appointments />}/>
              <Route path="*" element={<p>Not found</p>}/>
            </Routes>
          <Footer />
          
        </div>

      </HashRouter>
    </>
  );
}


export default App;
