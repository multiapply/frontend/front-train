import { BrowserRouter, Routes, Route} from 'react-router-dom'
import './App.css'

const NotImplemented = () => { 
  return <h1>Esta página aún no está lista</h1>
 }

function App() {

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={ <NotImplemented /> } />
      </Routes>
    </BrowserRouter>
  )
}

export default App
