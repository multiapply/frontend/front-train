import PropTypes from 'prop-types';
import { ChevronRightIcon } from '@heroicons/react/24/solid'


export const OrdersCard = ({totalPrice, totalProducts}) => {
  return (
    <div className="flex justify-between items-center | mb-4 p-4 w-80 | rounded-lg border border-black ">

        <section className="flex justify-between w-full">
          <article className="flex flex-col">
            <span className='font-light'>01.02.23</span>
            <span className='font-light'>{totalProducts} articles </span>
          </article>

          <article className='flex items-center gap-2'>
            <span className='font-medium text-2xl'>${totalPrice}</span>
            <ChevronRightIcon className='w-6 h-6 text-black-500 cursor-pointer'/>
          </article>
        </section>
        <p></p>
    </div>
  )
}

OrdersCard.propTypes = {
  totalPrice: PropTypes.number.isRequired,
  totalProducts: PropTypes.string.isRequired
}