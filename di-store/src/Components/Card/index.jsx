import PropTypes from 'prop-types';
import { PlusIcon, CheckIcon } from '@heroicons/react/24/solid'

import { useContext } from 'react';
import { ShoppingCartContext } from '../../Context';

export const Card = ({data}) => {
  const context = useContext(ShoppingCartContext)

  const showProduct = (productDetail) => { 
    context.closeCheckoutSideMenu()
    context.openProductDetail()
    context.setProductDetail(productDetail)
  }
  
  return (
    <div 
      className="mb-3 w-56 h-60 | bg-white cursor-pointer rounded-lg"
      onClick={() => showProduct(data)}>
      <figure className="relative mb-2 w-full h-4/5">
        <span className="absolute bottom-0 left-0 m-2 px-3 py-0.5 | rounded-lg bg-white/60 text-xs text-black">{data.category}</span>
        <img className="w-full h-full rounded-lg object-cover" src={data.image} alt="headphones" />

        <RenderIcon product={data} context={context}/>
      </figure>
      <p className="flex justify-between mx-2 | text-black">
        <span className="text-sm font-light">{data.title}</span>
        <span className="text-lg font-medium">${data.price}</span>
      </p>
    </div>
  )
}

Card.propTypes = {
  data: PropTypes.object.isRequired
}


const RenderIcon = ({product, context}) => {
  const addProductsToCart = (event, productData) => { 
    event.stopPropagation()
    context.setProductCount(context.productCount + 1)
    context.setCartProducts([...context.cartProducts, productData] )
    context.closeProductDetail()
    context.openCheckoutSideMenu()
  }

  const isInCart = context.cartProducts.some(item => item.id === product.id)
  if (isInCart) {
    return (
        <div 
          className="absolute top-0 right-0 flex justify-center items-center | m-2 p-1 w-6 h-6 | bg-black rounded-full text-black"
        >
          <CheckIcon className='w-6 h-6 text-white'></CheckIcon>
        </div>
    )
  } else {
    return (
        <div 
          className="absolute top-0 right-0 flex justify-center items-center | m-2 p-1 w-6 h-6 | bg-white rounded-full text-black"
          onClick={(event) => addProductsToCart(event,product)}
        >
          <PlusIcon className='w-6 h-6 text-black'></PlusIcon>
        </div>
    )
  }
}


RenderIcon.propTypes = {
  product: PropTypes.object.isRequired,
  context: PropTypes.object.isRequired

}