import { XMarkIcon } from '@heroicons/react/24/solid'
import { useContext } from 'react';
import { ShoppingCartContext } from '../../Context';
import { Link } from 'react-router-dom'
import { totalPrice } from '../../utils';
import './styles.css'
import { OrderCard } from '../OrderCard';

export const CheckoutSideMenu = () => {
  const context = useContext(ShoppingCartContext)
  // console.log('CART: ', context.cartProducts)
  
  const toggleSidebar = context.isCheckoutSideMenuOpen ? 'flex': 'hidden'
  
  const handleDelete = (id) => { 
    const filteredProducts = context.cartProducts.filter(product => product.id != id)
    context.setCartProducts(filteredProducts)
  }

  const handleCheckout = () => { 
    const orderToAdd = {
      id: crypto.randomUUID(),
      date: '01.02.2023',
      products: context.cartProducts,
      totalProducts: context.cartProducts.length,
      totalPrice: totalPrice(context.cartProducts),
    };
    context.setOrder([...context.order, orderToAdd]);
    context.setCartProducts([]);
    context.closeCheckoutSideMenu();

    // context.setCount(0);
  }

  return (
    <aside 
      className={`${toggleSidebar} checkout-side-menu flex flex-col fixed right-0 border border-black rounded-lg bg-white`}
      >
      <section className="flex justify-between items-center p-6 | text-black">
        <h2 className="font-medium text-xl">Detail</h2>
        <div>
          <XMarkIcon 
            className='w-6 h-6 text-black-500 cursor-pointer' 
            onClick={context.closeCheckoutSideMenu}></XMarkIcon>
        </div>
      </section>
      <section className='flex-1 px-6 overflow-y-scroll'>
      {
        context.cartProducts.map(product => (
          <OrderCard 
            key={product.id}
            id={product.id}
            title={product.title}
            imageUrl={product.image}
            price={product.price}
            handleDelete={handleDelete}
          />
        ))
      }
      </section>
      <footer className='px-6 mb-6'>
        <p className='flex justify-between items-center | mb-2'>
          <span className='font-light'>Total:</span>
          <span className='font-medium text-2xl'>${totalPrice(context.cartProducts)}</span>
        </p>
        <Link to='/my-orders/last'>
          <button className='py-3 w-full | rounded-lg text-white bg-black' onClick={handleCheckout}>Checkout</button>
        </Link>
      </footer>
    </aside>
  )
}
