import { XMarkIcon } from '@heroicons/react/24/solid'
import { useContext } from 'react';
import { ShoppingCartContext } from '../../Context';
import './styles.css'

export const ProductDetail = () => {
  const { 
    isProductDetailOpen,
    toggleProductDetail,
    productDetail
  } = useContext(ShoppingCartContext)

  const toggleSidebar = isProductDetailOpen ? 'flex': 'hidden'

  return (
    <aside 
      className={`${toggleSidebar} product-detail flex flex-col fixed right-0 border border-black rounded-lg bg-white`}
      >
      <div className="flex justify-between items-center p-6 | text-black">
        <h2 className="font-medium text-xl">Detail</h2>
        <div>
          <XMarkIcon 
            className='w-6 h-6 text-black-500 cursor-pointer' 
            onClick={toggleProductDetail}></XMarkIcon>
        </div>
      </div>
      <figure className='px-6 mx-auto'>
        <img 
          className='h-40 rounded-lg' 
          src={productDetail.image} 
          alt={productDetail.title} />
      </figure>
      <p className='flex flex-col p-6 text-black'>
        <span className='font-medium text-2xl mb-2'>${productDetail.price}</span>
        <span className='font-medium text-md'>{productDetail.title}</span>
        <span className='font-light text-sm'>{productDetail.description}</span>
      </p>
    </aside>
  )
}
