import PropTypes from 'prop-types';
import { XMarkIcon } from '@heroicons/react/24/solid'

export const OrderCard = ({id, title, imageUrl, price, handleDelete}) => {
  let renderXMarkIcon
  if (handleDelete) {
    renderXMarkIcon = <XMarkIcon 
    className='w-6 h-6 text-black-500 cursor-pointer'
    onClick={() => handleDelete(id)}></XMarkIcon>
  }
  return (
    <div className="flex justify-between items-center mb-3">
      <section className='flex items-center gap-2'>
        <figure className='w-20 h20'>
          <img className='w-full h-full rounded-lg object-cover' src={imageUrl} alt={title} />
        </figure>
        <p className='text-sm font-light'>{title}</p>
      </section>

      <section className='flex items-center gap-2'>
        <p className='text-lg font-medium'>{price}</p>
        {renderXMarkIcon}
      </section>
    </div>
  )
}

OrderCard.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  imageUrl: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  handleDelete: PropTypes.func.isRequired
}