import { useContext } from "react";
import { NavLink } from "react-router-dom"
import { ShoppingBagIcon } from '@heroicons/react/24/solid'

import { ShoppingCartContext } from '../../Context';


export const Navbar = () => {
  const context = useContext(ShoppingCartContext)
  const activeStyle = 'underline underline-offset-4'

  return (
    <nav className="fixed flex justify-between items-center | top-0 w-full z-10 | py-5 px-8 | text-sm font-light">
      <ul className="flex items-center gap-3">
        <li className="font-semibold text-lg">
          <NavLink to='/'>
            Shopi
          </NavLink>
        </li>
        <li>
          <NavLink to='/' onClick={() => context.setSearchedCategory(null)}
            className= {({isActive}) => isActive ? activeStyle : undefined }
          >
            All
          </NavLink>
        </li>
        <li>
          <NavLink to='/menclothes' onClick={() => context.setSearchedCategory("men's clothing")}
            className= {({isActive}) => isActive ? activeStyle : undefined }
          >
            Men Clothes
          </NavLink>
        </li>
        <li>
          <NavLink to='/womenclothes' onClick={() => context.setSearchedCategory("women's clothing")}
            className= {({isActive}) => isActive ? activeStyle : undefined }
          >
            Women Clothes
          </NavLink>
        </li>
        <li>
          <NavLink to='/electronics' onClick={() => context.setSearchedCategory("electronics")}
            className= {({isActive}) => isActive ? activeStyle : undefined }
          >
            Electronics
          </NavLink>
        </li>
        <li>
          <NavLink to='/jewelery' onClick={() => context.setSearchedCategory("jewelery")}
            className= {({isActive}) => isActive ? activeStyle : undefined }
          >
            Jewelery
          </NavLink>
        </li>
        {/* <li>
          <NavLink to='/others' onClick={() => context.setSearchedCategory('others')}
            className= {({isActive}) => isActive ? activeStyle : undefined }
          >
            Others
          </NavLink>
        </li> */}
      </ul>
      <ul className="flex items-center gap-3">
        <li className="text-white/60">
          user@gmail.com
        </li>
        <li>
          <NavLink to='/my-orders'
            className= {({isActive}) => isActive ? activeStyle : undefined }
          >
            My Orders
          </NavLink>
        </li>
        <li>
          <NavLink to='/my-account'
            className= {({isActive}) => isActive ? activeStyle : undefined }
          >
            Clothes
          </NavLink>
        </li>
        <li>
          <NavLink to='/sign-in'
            className= {({isActive}) => isActive ? activeStyle : undefined }
          >
            Sign In
          </NavLink>
        </li>
        <li className="flex items-center gap-1">
          <ShoppingBagIcon className='w-6 h-6 text-black-500'></ShoppingBagIcon>
          {context.productCount}
        </li>
      </ul>
    </nav>
  )
}
