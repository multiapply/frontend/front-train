import PropTypes from 'prop-types';
import { createContext, useEffect, useState } from "react";

export const ShoppingCartContext = createContext();

export const ShoppingCartProvider = ({children}) => {
  const [productCount, setProductCount] = useState(0)

  // Product Detail - Toggle Sidebar
  const [isProductDetailOpen, setIsProductDetailOpen] = useState(false)
  const toggleProductDetail = () => setIsProductDetailOpen(!isProductDetailOpen)
  const openProductDetail = () => setIsProductDetailOpen(true)
  const closeProductDetail = () => setIsProductDetailOpen(false)

  // Product Detail - Toggle Sidebar
  const [isCheckoutSideMenuOpen, setIsCheckoutSideMenuOpen] = useState(false)
  const openCheckoutSideMenu = () => setIsCheckoutSideMenuOpen(true)
  const closeCheckoutSideMenu = () => setIsCheckoutSideMenuOpen(false)

  // Product Detail - Show Product
  const [productDetail, setProductDetail] = useState({})

  // Shopping Cart - Add Products to Cart
  const [cartProducts, setCartProducts] = useState([])

  // Shopping Cart - Order
  const [order, setOrder] = useState([])

  // get Products
  const [productItems, setProductItems] = useState(null)
  const [filteredItems, setFilteredItems] = useState(null)
  // get Products by title - searchedTitle
  const [searchedTitle, setSearchedTitle] = useState(null)
  
  // get Products by category - searchedCategory
  const [searchedCategory, setSearchedCategory] = useState(null)

  // get from remote API
  useEffect(() => {
    fetch('https://fakestoreapi.com/products')
        .then(response => response.json())
        .then(data => setProductItems(data))
  }, [])

  // Filter Products by Title
  const filterItemsByTitle = (items, searchedTitle) => {
    return items?.filter( item => item.title.toLowerCase().includes(searchedTitle.toLowerCase()))
  }

  // Filter Products by Category
  const filterItemsByCategory = (items, searchedCategory) => {
    return items?.filter( item => item.category.toLowerCase() == searchedCategory.toLowerCase())
  }

  const filterBy = (searchType, productItems, searchedTitle, searchedCategory ) => {
    if (searchType === 'BY_TITLE') {
      return filterItemsByTitle(productItems, searchedTitle)
    }
    if (searchType === 'BY_CATEGORY') {
      return filterItemsByCategory(productItems, searchedCategory)
    }
    if (searchType === 'BY_TITLE_AND_CATEGORY') {
      return filterItemsByCategory(productItems, searchedCategory).filter(item => item.title.toLowerCase().includes(searchedTitle.toLowerCase()))
    }
    if (!searchType) {
      return productItems
    }
  }

  // React to search changes
  useEffect(()=> {
    if (searchedTitle && searchedCategory) setFilteredItems(filterBy('BY_TITLE_AND_CATEGORY', productItems, searchedTitle, searchedCategory))
    if (searchedTitle && !searchedCategory) setFilteredItems(filterBy('BY_TITLE', productItems, searchedTitle, searchedCategory))
    if (!searchedTitle && searchedCategory) setFilteredItems(filterBy('BY_CATEGORY', productItems, searchedTitle, searchedCategory))
    if (!searchedTitle && !searchedCategory) setFilteredItems(filterBy(null, productItems, searchedTitle, searchedCategory))
  }, [productItems, searchedTitle, searchedCategory])


  return (
    <ShoppingCartContext.Provider value={{
      productCount,
      setProductCount,
      isProductDetailOpen,
      toggleProductDetail,
      openProductDetail,
      closeProductDetail,
      productDetail,
      setProductDetail,
      cartProducts,
      setCartProducts,
      isCheckoutSideMenuOpen,
      openCheckoutSideMenu,
      closeCheckoutSideMenu,
      order,
      setOrder,
      productItems,
      setProductItems,
      searchedTitle,
      setSearchedTitle,
      filteredItems,
      searchedCategory,
      setSearchedCategory
    }}>
      {children}
    </ShoppingCartContext.Provider>
  )
}

ShoppingCartProvider.propTypes = {
  children: PropTypes.element.isRequired
};