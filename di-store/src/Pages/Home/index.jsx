import { useContext } from 'react'
import { Card } from "../../Components/Card"
import { ProductDetail } from '../../Components/ProductDetail'
import { ShoppingCartContext } from '../../Context' 

const ProductsList = () => { 
  const context = useContext(ShoppingCartContext)
      
  if (context.filteredItems?.length > 0) {
    return context.filteredItems.map(item => (
      <Card key={item.id} data={item} />
    ));
  } else {
    return <p>No Results Found</p>;
  }
}

export const Home = () => {
  const context = useContext(ShoppingCartContext)
  
  const handleSearch = (event) => {
    context.setSearchedTitle(event.target.value)
  }

  return (
    <>
      <div className='relative | flex justify-center items-center | w-80'>
        <h1 className='font-medium text-xl'>Exclusive Products</h1>
      </div>
      <input 
        className='mb-4 p-4 w-80 | rounded-lg border border-black | focus:outline-none'
        type="text" placeholder='Search a product'
        value={context.searchedTitle}
        onChange={handleSearch}/>

      <div className='grid gap-4 grid-cols-4 w-full max-w-screen-lg'>
        <ProductsList />
      </div>
      <ProductDetail />
    </>
  )
}


