import { Link, useParams } from 'react-router-dom';

import { useContext } from 'react';
import { ShoppingCartContext } from '../../Context';
import { OrderCard } from '../../Components/OrderCard';
import { ChevronLeftIcon } from '@heroicons/react/24/solid'


export const MyOrder = () => {
  const context = useContext(ShoppingCartContext)
  const { id : indexOrderPath } = useParams();
  const orderToShow = indexOrderPath === undefined ? context.order?.slice(-1)[0] : context.order?.filter(order => order.id === indexOrderPath)[0] 
  return (
    <div>
      <div className='relative | flex justify-center items-center | w-80 mb-6'>
        <Link to='/my-orders' className='absolute left-0'>
          <ChevronLeftIcon className='w-6 h-6 text-black-500 cursor-pointer' />
        </Link>
        <h1>My Order</h1>
      </div>
      <section className='flex flex-col w-80'>
      {
        orderToShow?.products.map(product => (
          <OrderCard 
            key={product.id}
            id={product.id}
            title={product.title}
            imageUrl={product.image}
            price={product.price}
          />
        ))
      }
      </section>
    </div>
  )
}
