import { Link } from 'react-router-dom';
import { useContext } from 'react';
import { ShoppingCartContext } from '../../Context';
import { OrdersCard } from "../../Components/OrdersCard"


export const MyOrders = () => {
  const context = useContext(ShoppingCartContext)
  return (
    <div>
      <div className='relative | flex justify-center items-center | w-80'>
        <h1 className='font-medium text-xl'>My Orders</h1>
      </div>
      
      {
        context.order.map((order) => (
          <Link key={order.id} to={`/my-orders/${order.id}`}>
            <OrdersCard totalPrice={order.totalPrice} totalProducts={order.totalProducts} />
          </Link>
        ))
      }
    </div>
  )
}
